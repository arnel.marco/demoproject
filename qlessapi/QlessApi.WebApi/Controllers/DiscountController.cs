﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Qless.Services.Qless.Commands;
using Qless.Services.Qless.Dto;
using Qless.Services.Qless.Queries;
using System.Threading.Tasks;

namespace Qless.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DiscountController : ControllerBase
    {

        private readonly IMediator _mediator;

        public DiscountController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var response = await _mediator.Send(new GetDiscounts()).ConfigureAwait(false);
            return Ok(response);
        }


        // GET api/values/5
        [HttpGet("{discountId}")]
        public async Task<IActionResult> Get(int discountId)
        {
            var response = await _mediator.Send(new GetDiscount(discountId)).ConfigureAwait(false);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] DiscountDto request)
        {
            var response = await _mediator.Send(new AddDiscount(request)).ConfigureAwait(false);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult>  Put([FromBody] DiscountDto request)
        {
            var response = await _mediator.Send(new UpdateDiscount(request)).ConfigureAwait(false);
            return Ok(response);
        }
  
        [HttpDelete("{discountId}")]
        public async Task<IActionResult> Delete(int discountId)
        {
            var response = await _mediator.Send(new DeleteDiscount(discountId)).ConfigureAwait(false);
            return Ok(response);
        }
    }
}
