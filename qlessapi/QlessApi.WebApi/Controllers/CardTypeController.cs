﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Qless.Services.Qless.Commands;
using Qless.Services.Qless.Dto;
using Qless.Services.Qless.Queries;
using System.Threading.Tasks;

namespace Qless.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardTypeController : ControllerBase
    {

        private readonly IMediator _mediator;

        public CardTypeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var response = await _mediator.Send(new GetCardTypes()).ConfigureAwait(false);
            return Ok(response);
        }

        [HttpGet("{cardTypeId}")]
        public async Task<IActionResult> Get(int cardTypeId)
        {
            var response = await _mediator.Send(new GetCardType(cardTypeId)).ConfigureAwait(false);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CardTypeDto request)
        {
            var response = await _mediator.Send(new AddCardType(request)).ConfigureAwait(false);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] CardTypeDto request)
        {
            var response = await _mediator.Send(new UpdateCardType(request)).ConfigureAwait(false);
            return Ok(response);
        }

        [HttpDelete("{cardTypeId}")]
        public async Task<IActionResult> Delete(int cardTypeId)
        {
            var response = await _mediator.Send(new DeleteCardType(cardTypeId)).ConfigureAwait(false);
            return Ok(response);
        }

    }
}
