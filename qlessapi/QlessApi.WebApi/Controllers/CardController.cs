﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Qless.Services.Qless.Commands;
using Qless.Services.Qless.Dto;
using Qless.Services.Qless.Queries;
using System.Threading.Tasks;

namespace Qless.WebApi.Controllers
{
    /// <summary>
    /// Developer should have familiarity whith clean code architecture, factory pattern, SOLID and CSQRS MediatR
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CardController : ControllerBase
    {

        private readonly IMediator _mediator;

        public CardController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var response = await _mediator.Send(new GetCards()).ConfigureAwait(false);
            return Ok(response);
        }
     
        [HttpGet("{cardId}")]
        public async Task<IActionResult> Get(int cardId)
        {
            var response = await _mediator.Send(new GetCard(cardId)).ConfigureAwait(false);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CardDto request)
        {
            var response = await _mediator.Send(new AddCard(request)).ConfigureAwait(false);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] CardDto request)
        {
            var response = await _mediator.Send(new UpdateCard(request)).ConfigureAwait(false);
            return Ok(response);
        }

        [HttpDelete("{cardId}")]
        public async Task<IActionResult> Delete(int cardId)
        {
            var response = await _mediator.Send(new DeleteCard(cardId)).ConfigureAwait(false);
            return Ok(response);
        }
    }
}
