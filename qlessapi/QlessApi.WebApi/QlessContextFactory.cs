﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Qless.DAL.Context;
using System.IO;

namespace Qless.WebApi
{
    public class QlessContextFactory : IDesignTimeDbContextFactory<QlessContext>
    {      
        public QlessContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json")
             .Build();

            var connectionString = configuration.GetConnectionString("QlessEntities");

            var optionBuilder = new DbContextOptionsBuilder<QlessContext>();
            
            optionBuilder.UseSqlServer(connectionString);
            return new QlessContext(optionBuilder.Options);
        }
    }
}
