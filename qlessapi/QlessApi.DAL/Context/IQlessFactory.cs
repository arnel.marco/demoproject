﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qless.DAL.Context
{
    public interface IQlessFactory
    {
        QlessContext Create();
    }
}
