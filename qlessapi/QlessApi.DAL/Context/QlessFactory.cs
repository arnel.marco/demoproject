﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qless.DAL.Context
{
    public class QlessFactory : IQlessFactory
    {
        private readonly string _connectionString;

        public QlessFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public QlessContext Create()
        {
            return new QlessContext(_connectionString);
        }      
      
    }
}
