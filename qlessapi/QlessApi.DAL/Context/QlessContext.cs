﻿using Qless.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Qless.DAL.Context
{
    public class QlessContext : DbContext
    {
        public QlessContext(DbContextOptions<QlessContext> options) : base(options)
        {
        }

        public QlessContext(string connectionString)
            : base(new DbContextOptionsBuilder().UseSqlServer(connectionString).Options)
        {
        }

        public DbSet<Card> Card { get; set; }
        public DbSet<Discount> Discount { get; set; }
        public DbSet<CardType> CardType { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
            //modelBuilder.Entity<CardType>().HasData(
            //new { CardTypeId = 1, CardName = "Regular", IsDeleted = false },
            //new { CardTypeId = 2, CardName = "Discounted", IsDeleted = false });
        //}       

    }
}
