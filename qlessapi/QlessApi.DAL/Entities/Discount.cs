﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Qless.DAL.Entities
{
    public class Discount
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DiscountId { get; set; }
        public string DiscountName { get; set; }
        public int PrincipalDiscount { get; set; }
        public int AdditionalDiscount { get; set; }
        public int NumberOfDiscountPerDay { get; set; }
        public bool IsDeleted { get; set; }
    }
}
