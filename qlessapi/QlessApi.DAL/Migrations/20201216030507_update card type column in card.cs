﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QlessApi.DAL.Migrations
{
    public partial class updatecardtypecolumnincard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Card_CardType_CardTypeId",
                table: "Card");

            migrationBuilder.DropIndex(
                name: "IX_Card_CardTypeId",
                table: "Card");

            migrationBuilder.DropColumn(
                name: "CardTypeId",
                table: "Card");

            migrationBuilder.AddColumn<int>(
                name: "CardType",
                table: "Card",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CardType",
                table: "Card");

            migrationBuilder.AddColumn<int>(
                name: "CardTypeId",
                table: "Card",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Card_CardTypeId",
                table: "Card",
                column: "CardTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Card_CardType_CardTypeId",
                table: "Card",
                column: "CardTypeId",
                principalTable: "CardType",
                principalColumn: "CardTypeId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
