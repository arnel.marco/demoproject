﻿using Qless.Services.Qless.Dto;
using MediatR;

namespace Qless.Services.Qless.Commands
{
    public class DeleteDiscount : IRequest<DiscountDto>
    {
        public int DiscountId { get; set; }

        public DeleteDiscount(int discountId)
        {
            DiscountId = discountId;
        }
    }
}
