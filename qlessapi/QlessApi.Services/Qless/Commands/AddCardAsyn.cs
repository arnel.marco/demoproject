﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Commands
{
    public class AddCardAsync : IRequestHandler<AddCard, CardDto>
    {
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public AddCardAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }
        public async Task<CardDto> Handle(AddCard request, CancellationToken cancellationToken)
        {
            // not needed try catch block here it will be handle by global error handler and the validation
            using (var context = _qlessFactory.Create())
            {               
                var card = _mapper.Map<Card>(request.CardDto);
                context.Card.Add(card);
                await context.SaveChangesAsync();
                return _mapper.Map<CardDto>(card);
            }
        }
    }
}
