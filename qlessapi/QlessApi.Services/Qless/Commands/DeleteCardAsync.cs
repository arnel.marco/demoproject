﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Qless.DAL.Context;
using Qless.Services.Qless.Dto;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Commands
{

    public class DeleteCardAsync : IRequestHandler<DeleteCard, CardDto>
    {
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public DeleteCardAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }

        public async Task<CardDto> Handle(DeleteCard request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {

                var  card = await context.Card.Where(x => x.CardId == request.CardId).FirstOrDefaultAsync();
                card.IsDeleted = true;
                context.Update(card);
                await context.SaveChangesAsync();
                return _mapper.Map<CardDto>(card);
            }
        }
    }
}
