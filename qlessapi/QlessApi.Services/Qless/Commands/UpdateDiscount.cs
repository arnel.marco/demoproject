﻿using Qless.Services.Qless.Dto;
using MediatR;

namespace Qless.Services.Qless.Commands
{
    public class UpdateDiscount : IRequest<DiscountDto>
    {

        public DiscountDto Discount { get; set; }


        public UpdateDiscount(DiscountDto request)
        {
            Discount = request;
        }
    }
}
