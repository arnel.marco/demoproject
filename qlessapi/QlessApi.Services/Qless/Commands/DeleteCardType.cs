﻿using Qless.Services.Qless.Dto;
using MediatR;

namespace Qless.Services.Qless.Commands
{
    public class DeleteCardType : IRequest<CardTypeDto>
    {
        public int CardTypeId { get; set; }

        public DeleteCardType(int cardTypeId)
        {
            CardTypeId = cardTypeId;
        }
    }
}
