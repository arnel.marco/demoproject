﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Commands
{
    public class UpdateCardTypeAsync : IRequestHandler<UpdateCardType, CardTypeDto>
    {
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public UpdateCardTypeAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }

        public async Task<CardTypeDto> Handle(UpdateCardType request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {
                var cartdType = await context.CardType.Where(x => x.CardTypeId == request.CardType.CardTypeId).FirstOrDefaultAsync();
                _mapper.Map(request.CardType, cartdType);
                context.Update(cartdType);
                await context.SaveChangesAsync();
                return _mapper.Map<CardTypeDto>(cartdType);
            }
        }
    }
}
