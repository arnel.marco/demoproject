﻿using Qless.Services.Qless.Dto;
using MediatR;

namespace Qless.Services.Qless.Commands
{
    public class UpdateCard : IRequest<CardDto>
    {

        public CardDto Card { get; set; }


        public UpdateCard(CardDto request)
        {
            Card = request;
        }
    }
}
