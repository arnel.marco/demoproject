﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Commands
{
    public class UpdateDiscountAsync : IRequestHandler<UpdateDiscount, DiscountDto>
    {
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public UpdateDiscountAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }

        public async Task<DiscountDto> Handle(UpdateDiscount request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {

                var discount = await context.Discount.Where(x => x.DiscountId == request.Discount.DiscountId).FirstOrDefaultAsync();
                _mapper.Map(request.Discount, discount);
                context.Update(discount);
                await context.SaveChangesAsync();
                return _mapper.Map<DiscountDto>(discount);
            }
        }
    }
}
