﻿using Qless.Services.Qless.Dto;
using MediatR;

namespace Qless.Services.Qless.Commands
{
    public class AddCardType : IRequest<CardTypeDto>
    {
        public CardTypeDto CardTypeDto { get; set; }

        public AddCardType(CardTypeDto cardTypeDto)
        {
            CardTypeDto = cardTypeDto;
        }
    }
}
