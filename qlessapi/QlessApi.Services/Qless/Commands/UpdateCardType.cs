﻿using Qless.Services.Qless.Dto;
using MediatR;

namespace Qless.Services.Qless.Commands
{
    public class UpdateCardType : IRequest<CardTypeDto>
    {

        public CardTypeDto CardType { get; set; }


        public UpdateCardType(CardTypeDto cardTypeDto)
        {
            CardType = cardTypeDto;
        }
    }
}
