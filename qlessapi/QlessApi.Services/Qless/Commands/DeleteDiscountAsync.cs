﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Qless.DAL.Context;
using Qless.Services.Qless.Dto;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Commands
{

    public class DeleteDiscountAsync : IRequestHandler<DeleteDiscount, DiscountDto>
    {
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public DeleteDiscountAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }

        public async Task<DiscountDto> Handle(DeleteDiscount request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {

                var  discount = await context.Discount.Where(x => x.DiscountId == request.DiscountId).FirstOrDefaultAsync();
                discount.IsDeleted = true;
                context.Update(discount);
                await context.SaveChangesAsync();
                return _mapper.Map<DiscountDto>(discount);
            }
        }
    }
}
