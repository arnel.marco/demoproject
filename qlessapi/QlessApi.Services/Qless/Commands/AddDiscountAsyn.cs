﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Commands
{
    public class AddDiscountAsync : IRequestHandler<AddDiscount, DiscountDto>
    {
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public AddDiscountAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }
        public async Task<DiscountDto> Handle(AddDiscount request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {
                var discount =  _mapper.Map<Discount>(request.Discount);
                context.Discount.Add(discount);
                await context.SaveChangesAsync();
                return  _mapper.Map<DiscountDto>(discount);
            }
        }
    }
}
