﻿using Qless.Services.Qless.Dto;
using MediatR;

namespace Qless.Services.Qless.Commands
{
    public class AddCard : IRequest<CardDto>
    {
        public CardDto CardDto { get; set; }

        public AddCard(CardDto cardDto)
        {
            CardDto = cardDto;
        }
    }
}
