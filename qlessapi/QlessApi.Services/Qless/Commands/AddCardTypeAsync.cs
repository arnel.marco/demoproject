﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Commands
{
    public class AddCardTypeAsync : IRequestHandler<AddCardType, CardTypeDto>
    {
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public AddCardTypeAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }
        public async Task<CardTypeDto> Handle(AddCardType request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {
                var cardType = _mapper.Map<CardType>(request.CardTypeDto);
                context.CardType.Add(cardType);
                await context.SaveChangesAsync();
                return _mapper.Map<CardTypeDto>(cardType);
            }
        }
    }
}
