﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Commands
{
    public class UpdateCardAsync : IRequestHandler<UpdateCard, CardDto>
    {
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public UpdateCardAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }

        public async Task<CardDto> Handle(UpdateCard request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {

                var card = await context.Card.Where(x => x.CardId== request.Card.CardId).FirstOrDefaultAsync();
                _mapper.Map(request.Card, card);
                context.Update(card);
                await context.SaveChangesAsync();
                return _mapper.Map<CardDto>(card);
            }
        }
    }
}
