﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.Services.Qless.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Commands
{
 
    public class DeleteCardTypeAsync : IRequestHandler<DeleteCardType, CardTypeDto>
    {
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public DeleteCardTypeAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }

        public async Task<CardTypeDto> Handle(DeleteCardType request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {
                var  cardType = await context.CardType.Where(x => x.CardTypeId == request.CardTypeId).FirstOrDefaultAsync();
                cardType.IsDeleted = true;
                context.Update(cardType);
                await context.SaveChangesAsync();
                return _mapper.Map<CardTypeDto>(cardType);
            }
        }
    }
}
