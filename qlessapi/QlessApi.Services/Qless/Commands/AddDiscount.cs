﻿using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Qless.Services.Qless.Commands
{
    public class AddDiscount : IRequest<DiscountDto>
    {               
        public DiscountDto Discount { get; set; }


        public AddDiscount(DiscountDto request)
        {
            Discount = request;
        }

    
    }
}
