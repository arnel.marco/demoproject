﻿using Qless.Services.Qless.Dto;
using MediatR;

namespace Qless.Services.Qless.Commands
{
    public class DeleteCard : IRequest<CardDto>
    {
        public int CardId { get; set; }

        public DeleteCard(int cardId)
        {
            CardId = cardId;
        }
    }
}
