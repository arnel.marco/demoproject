﻿using FluentValidation;
using Qless.Services.Qless.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace QlessApi.Services.Qless.Validators
{
    public class GetCardValidator : AbstractValidator<GetCard>
    {
        public GetCardValidator()
        {
            RuleFor(s => s .CardId).SetValidator(new IdValidator());
        }
    }
}
