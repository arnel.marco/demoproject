﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Qless.Services.Qless.Commands;
using Qless.Services.Qless.Dto;

namespace QlessApi.Services.Qless.Validators
{
    public class DeleteTypeValidator : AbstractValidator<DeleteCardType>
    {
        public DeleteTypeValidator()
        {
            RuleFor(s => s.CardTypeId).SetValidator(new IdValidator());
        }
    }
}
