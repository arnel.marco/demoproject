﻿using FluentValidation;
using Qless.Services.Qless.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace QlessApi.Services.Qless.Validators
{
    public class GetCardTypeValidator : AbstractValidator<GetCardType>
    {
        public GetCardTypeValidator()
        {
            RuleFor(s => s.CardTypeId).SetValidator(new IdValidator());
        }
    }
}
