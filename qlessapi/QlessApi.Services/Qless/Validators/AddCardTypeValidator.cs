﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Qless.Services.Qless.Commands;
using Qless.Services.Qless.Dto;

namespace QlessApi.Services.Qless.Validators
{
    public class AddCardTypeValidator : AbstractValidator<AddCardType>
    {
        public AddCardTypeValidator()
        {            
            RuleFor(s => s.CardTypeDto.Name).NotEmpty().NotNull();            
        }
    }
}
