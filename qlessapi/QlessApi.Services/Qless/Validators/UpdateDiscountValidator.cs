﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Qless.Services.Qless.Commands;
using Qless.Services.Qless.Dto;

namespace QlessApi.Services.Qless.Validators
{
    public class UpdateDiscountValidator : AbstractValidator<UpdateDiscount>
    {
        public UpdateDiscountValidator()
        {            
            RuleFor(s => s.Discount.DiscountName).NotEmpty().NotNull();
            RuleFor(s => s.Discount.IsDeleted).NotEmpty().NotNull();
            RuleFor(s => s.Discount.NumberOfDiscountPerDay).GreaterThan(x => 0).LessThanOrEqualTo(4).NotEmpty().NotNull();
            RuleFor(s => s.Discount.PrincipalDiscount).GreaterThan(0).NotEmpty().NotNull();
            RuleFor(s => s.Discount.AdditionalDiscount).GreaterThan(0).NotEmpty().NotNull();            
        }
    }
}
