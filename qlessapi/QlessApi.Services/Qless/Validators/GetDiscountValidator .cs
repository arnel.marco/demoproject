﻿using FluentValidation;
using Qless.Services.Qless.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace QlessApi.Services.Qless.Validators
{
    public class GetDiscountValidator : AbstractValidator<GetDiscount>
    {
        public GetDiscountValidator()
        {
            RuleFor(s => s .DiscountId).SetValidator(new IdValidator());
        }
    }
}
