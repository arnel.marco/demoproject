﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Qless.Services.Qless.Commands;
using Qless.Services.Qless.Dto;

namespace QlessApi.Services.Qless.Validators
{
    public class UpdateCardTypeValidator : AbstractValidator<UpdateCardType>
    {
        public UpdateCardTypeValidator()
        {            
            RuleFor(s => s.CardType.Name).NotEmpty().NotNull();            
        }
    }
}
