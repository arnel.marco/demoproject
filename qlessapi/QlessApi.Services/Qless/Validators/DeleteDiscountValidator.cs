﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Qless.Services.Qless.Commands;
using Qless.Services.Qless.Dto;

namespace QlessApi.Services.Qless.Validators
{
    public class DeleteDiscountValidator : AbstractValidator<DeleteDiscount>
    {
        public DeleteDiscountValidator()
        {
            RuleFor(s => s.DiscountId).SetValidator(new IdValidator());
        }
    }
}
