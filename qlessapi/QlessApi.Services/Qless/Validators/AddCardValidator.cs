﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Qless.Services.Qless.Commands;
using Qless.Services.Qless.Dto;

namespace QlessApi.Services.Qless.Validators
{
    public class AddCardValidator : AbstractValidator<AddCard>
    {
        public AddCardValidator()
        {            
            RuleFor(s => s.CardDto.CardType).NotEmpty().NotNull();
            RuleFor(s => s.CardDto.IssuedDate).NotEmpty().NotNull();
            RuleFor(s => s.CardDto.ExpiryDate).GreaterThan(x => x.CardDto.IssuedDate).NotEmpty().NotNull();
            RuleFor(s => s.CardDto.NumberYearsValid).GreaterThanOrEqualTo(1).NotEmpty().NotNull();
            RuleFor(s => s.CardDto.Load).GreaterThan(0).LessThanOrEqualTo(10000).NotEmpty().NotNull();            
        }
    }
}
