﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Qless.Services.Qless.Commands;
using Qless.Services.Qless.Dto;

namespace QlessApi.Services.Qless.Validators
{
    public class UpdateCardValidator : AbstractValidator<UpdateCard>
    {
        public UpdateCardValidator()
        {            
            RuleFor(s => s.Card.CardType).NotEmpty().NotNull();
            RuleFor(s => s.Card.IssuedDate).NotEmpty().NotNull();
            RuleFor(s => s.Card.ExpiryDate).GreaterThan(x => x.Card.IssuedDate).NotEmpty().NotNull();
            RuleFor(s => s.Card.NumberYearsValid).GreaterThanOrEqualTo(1).NotEmpty().NotNull();
            RuleFor(s => s.Card.Load).GreaterThan(0).LessThanOrEqualTo(10000).NotEmpty().NotNull();            
        }
    }
}
