﻿using Qless.DAL.Entities;
using System;

namespace Qless.Services.Qless.Dto
{
    public class CardDto
    {
        public int CardId { get; set; }
        public DateTime IssuedDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int NumberYearsValid { get; set; }
        public int Load { get; set; }
        public int CardType { get; set; }
        public bool IsDeleted { get; set; }

    }
}
