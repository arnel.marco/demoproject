﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qless.Services.Qless.Dto
{
    public class CardTypeDto
    {
        public int CardTypeId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
    }
}
