﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qless.Services.Qless.Dto
{
    public class DiscountDto
    {
        public int DiscountId { get; set; }
        public string DiscountName { get; set; }
        public int PrincipalDiscount { get; set; }
        public int AdditionalDiscount { get; set; }
        public int NumberOfDiscountPerDay { get; set; }
        public bool IsDeleted { get; set; }
    }
}
