﻿using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;

namespace Qless.Services.Qless.Queries
{
    public class GetDiscount : IRequest<DiscountDto>
    {
        public int DiscountId { get; set; }

        public GetDiscount(int discountId)
        {
            DiscountId = discountId;
        }
    }
}
