﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Queries
{
    public class GetDiscountAsync :  IRequestHandler<GetDiscount, DiscountDto>
    {         
         private readonly IQlessFactory _qlessFactory;
         private readonly IMapper _mapper;

        public GetDiscountAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
                _qlessFactory = QlessFactory;
                _mapper = mapper;
        }

  
        public async Task<DiscountDto> Handle(GetDiscount request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {
                var result = await context.Discount.Where(x => x.DiscountId == request.DiscountId).AsNoTracking().FirstOrDefaultAsync();                                        
                return  _mapper.Map<DiscountDto> (result);
            }
        }

    }
}
