﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Queries
{
    public class GetCardsAsync : IRequestHandler<GetCards, CardDto[]>
    {
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public GetCardsAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }

        public async Task<CardDto[]> Handle(GetCards request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {
                var result = await context.Card.AsNoTracking().ToArrayAsync();

                return _mapper.Map<CardDto[]>(result);
            }
        }
    }
}
