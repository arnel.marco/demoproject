﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.Services.Qless.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace Qless.Services.Qless.Queries
{
    public class GetCardTypeAsync :  IRequestHandler<GetCardType, CardTypeDto>
    {         
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public GetCardTypeAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }
  

        public async Task<CardTypeDto> Handle(GetCardType request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {

                var result = await context.CardType.Where(x => x.CardTypeId == request.CardTypeId).AsNoTracking().FirstOrDefaultAsync();
                return _mapper.Map<CardTypeDto>(result);
            }
        }
    }
}
