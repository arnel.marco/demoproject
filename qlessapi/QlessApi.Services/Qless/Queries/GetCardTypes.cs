﻿using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;

namespace Qless.Services.Qless.Queries
{
    public class GetCardTypes : IRequest<CardTypeDto[]>
    {
    }
}
