﻿using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;

namespace Qless.Services.Qless.Queries
{
    public class GetCardType : IRequest<CardTypeDto>          
    {
        public int CardTypeId { get; set; }
        public GetCardType(int cardTypeId)
        {
            CardTypeId = cardTypeId;        
        }
    }
}
