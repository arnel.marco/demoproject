﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Queries
{
    public class GetDiscountsAsync :  IRequestHandler<GetDiscounts, DiscountDto[]>
    {         
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public GetDiscountsAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }
  

        public async Task<DiscountDto[]> Handle(GetDiscounts request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {
                var result = await context.Discount.AsNoTracking()
                    .ToArrayAsync();

                return _mapper.Map<DiscountDto[]>(result);
            }
        }
    }
}
