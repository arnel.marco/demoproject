﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.Services.Qless.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Queries
{
    public class GetCardAsync : IRequestHandler<GetCard, CardDto>
    {        
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public GetCardAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
    }

        public async Task<CardDto> Handle(GetCard request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {
                var result = await context.Card.Where(x=>x.CardId == request.CardId).AsNoTracking().FirstOrDefaultAsync();

                return _mapper.Map<CardDto>(result);
            }
        }
       
    }
}
