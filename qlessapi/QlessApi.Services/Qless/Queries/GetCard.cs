﻿using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qless.Services.Qless.Queries
{
    public class GetCard : IRequest<CardDto>
    {        
        public int CardId { get; set; }

        public GetCard(int cardId)
        {
            CardId = cardId;
        }
    }
}
