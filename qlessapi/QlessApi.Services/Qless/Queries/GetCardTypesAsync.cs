﻿using AutoMapper;
using Qless.DAL.Context;
using Qless.Services.Qless.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Qless.Services.Qless.Queries
{
    public class GetCardTypesAsync :  IRequestHandler<GetCardTypes, CardTypeDto[]>
    {         
        private readonly IQlessFactory _qlessFactory;
        private readonly IMapper _mapper;

        public GetCardTypesAsync(IQlessFactory QlessFactory, IMapper mapper)
        {
            _qlessFactory = QlessFactory;
            _mapper = mapper;
        }
  

        public async Task<CardTypeDto[]> Handle(GetCardTypes request, CancellationToken cancellationToken)
        {
            using (var context = _qlessFactory.Create())
            {
                var result = await context.CardType.AsNoTracking()
                    .ToArrayAsync();
              
                return _mapper.Map<CardTypeDto[]>(result);
            }
        }
    }
}
