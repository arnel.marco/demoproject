﻿using AutoMapper;
using Qless.DAL.Entities;
using Qless.Services.Qless.Dto;

namespace Qless.Services
{
    public class ServiceMapperProfile : Profile
    {
        public ServiceMapperProfile()
        {
            CreateMapForQless();
        }

        private void CreateMapForQless()
        {
            CreateMap<Discount, DiscountDto>().ReverseMap();
            CreateMap<Card, CardDto>().ReverseMap();
            CreateMap<CardType, CardTypeDto>().ReverseMap();
        }
    }
}
