﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Qless.DAL.Context;
using Qless.Services.Qless.Commands;
using Qless.Services.Qless.Dto;
using Qless.Services.Qless.Queries;
using QlessApi.Services.Qless.Validators;

namespace Qless.Services
{
    public static class QlessService
    {
        public static IServiceCollection AddQlessServices(this IServiceCollection services)
        {
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            QlessValidators(services);            
            return services;
        }
        
        private static void QlessValidators(IServiceCollection services)
        {            
            services.AddTransient<IValidator<AddCardType>, AddCardTypeValidator>();
            services.AddTransient<IValidator<UpdateCardType>, UpdateCardTypeValidator>();
            services.AddTransient<IValidator<DeleteCardType>, DeleteTypeValidator>();
            services.AddTransient<IValidator<int>, IdValidator>();
            services.AddTransient<IValidator<GetCardType>, GetCardTypeValidator>();
            services.AddTransient<IValidator<AddCard>, AddCardValidator>();
            services.AddTransient<IValidator<UpdateCard>, UpdateCardValidator>();
            services.AddTransient<IValidator<DeleteCard>, DeleteCardValidator>();
            services.AddTransient<IValidator<GetCard>, GetCardValidator>();
            services.AddTransient<IValidator<AddDiscount>, AddDiscountValidator>();
            services.AddTransient<IValidator<UpdateDiscount>, UpdateDiscountValidator>();
            services.AddTransient<IValidator<DeleteDiscount>, DeleteDiscountValidator>();
            services.AddTransient<IValidator<GetDiscount>, GetDiscountValidator>();
        }     
    }
}
