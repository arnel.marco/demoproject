using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Qless.DAL.Context;
using Qless.DAL.Entities;
using Qless.Services.Qless.Commands;
using Qless.Services.Qless.Queries;
using Qless.Services.Qless.Dto;
using System;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using Qless.Services;
using System.Threading;
using QlessApi.Services.Qless.Validators;

namespace Qless.UnitTest
{
    /// <summary>
    /// Sample unit test 
    /// </summary>
    public class CardUnitTest
    {
        private readonly IQlessFactory _qlessFactory;        
        private readonly AddCardAsync _addCardAsync;
        private readonly GetCardsAsync _getCardsAsync;

        public CardUnitTest()
        {
            _qlessFactory = Substitute.For<IQlessFactory>();
            _mediator = Substitute.For<IMediator>();
            Mapper.Reset();
            Mapper.Initialize(cfg=> { cfg.AddProfile(new ServiceMapperProfile()); });
            var config = new MapperConfiguration(cfg => cfg.AddProfile<ServiceMapperProfile>());
            var _mapper = config.CreateMapper();
            _addCardAsync = new AddCardAsync(_qlessFactory, _mapper);
            _getCardsAsync = new GetCardsAsync(_qlessFactory, _mapper);
            SetupMocking();
        }

        private void SetupMocking()
        {
            var options = new DbContextOptionsBuilder<QlessContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            _qlessFactory.Create().Returns(new QlessContext(options));

            using (var context = new QlessContext(options))
            {
                var cardTypeRegular = new CardType()
                {
                    CardTypeId = 1,
                    Name = "Regular",
                    IsDeleted = false
                };

                var card = new Card()
                {
                    CardId = 1,
                    IssuedDate = new DateTime(),
                    ExpiryDate = new DateTime().AddYears(5),
                    Load = 100,
                    IsDeleted = false,
                    NumberYearsValid = 5,
                    CardType = cardTypeRegular.CardTypeId
                };
                context.Card.Add(card);
                context.SaveChanges();
            }
        }

        [Fact]
        public async Task CreateAllParametersValid()
        {
            //arange
            var cardTypeDiscounted = new CardTypeDto()
            {
                CardTypeId = 2,
                Name = "Discounted",
                IsDeleted = false
            };

            var card = new CardDto()
            {
                CardId = 2,
                IssuedDate = new DateTime(),
                ExpiryDate = new DateTime().AddYears(5),
                Load = 100,
                IsDeleted = false,
                NumberYearsValid = 5,
                CardType = cardTypeDiscounted.CardTypeId
            };
            var request = new AddCard(card);
            CardDto newCard = null;

            var _addCardValidator = new AddCardValidator();

            //act
            //manual call to  validation was handle by mediatr pipeline
            var validationResult = await _addCardValidator.ValidateAsync(request);
            if (validationResult.IsValid)
            {
                var response = await _addCardAsync.Handle(request, default(CancellationToken));
                newCard = Mapper.Map<CardDto>(response);
            }

            //assert
            validationResult.IsValid.Equals(true);
            newCard.Should().NotBeNull();
            newCard.CardId.Should().NotBe(0);
        }

        [Fact]
        public async Task CreateWithInvalidParameter()
        {
            //arange
            var cardTypeDiscounted = new CardTypeDto()
            {
                CardTypeId = 3,
                Name = "Discounted",
                IsDeleted = false
            };

            var card = new CardDto()
            {
                CardId = 3,
                IssuedDate = new DateTime().AddYears(5),
                ExpiryDate = new DateTime(),
                Load = 0,
                IsDeleted = false,
                NumberYearsValid = 0,
                CardType = cardTypeDiscounted.CardTypeId
            };
            var request = new AddCard(card);
            CardDto newCard = null;
            var _addCardValidator = new AddCardValidator();

            //act
            //manual call to  validation was handle by mediatr pipeline
            var validationResult = await _addCardValidator.ValidateAsync(request); 
            if (validationResult.IsValid)
            {
                var response = await _addCardAsync.Handle(request, default(CancellationToken));
                newCard = Mapper.Map<CardDto>(response);
            }

            //assert
            validationResult.IsValid.Equals(false);
            newCard.Should().BeNull();
        }

        [Fact]
        public async Task GetCardsWithShouldBeSuccess()
        {
            //arange
            CardDto[] cards = null;

            //act                
            var response = await _getCardsAsync.Handle(null, default(CancellationToken));
            cards = Mapper.Map<CardDto[]>(response);

            //assert            
            cards.Length.Should().BeGreaterThan(0);
        }


    }
}
