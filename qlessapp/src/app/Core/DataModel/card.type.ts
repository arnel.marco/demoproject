export interface ICardType {
    cardTypeId: number;
    isDeleted : Boolean;
    name: string;
    recordStatus: string;
}
