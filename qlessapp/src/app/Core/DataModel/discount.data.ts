export interface IDiscountData {
  discountId: number;
  discountName: string;
  principalDiscount: number;
  additionalDiscount: number;
  numberOfDiscountPerDay :number,
  isDeleted : Boolean,
  recordStatus: String;
}
