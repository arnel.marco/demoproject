import { ICardType } from './card.type';

export interface ICardData {
    cardId: number;
    issuedDate: string;
    expiryDate: string;
    numberYearsValid: number;
    load: number;
    cardType :number,
    isDeleted : Boolean,
    recordStatus: String;
}
