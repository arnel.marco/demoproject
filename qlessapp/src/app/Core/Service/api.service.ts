import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { environment as env } from 'src/environments/environment';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

const BASE_URL = env.apiConfig.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
  constructor(private _httpClient: HttpClient) { }

  
  public get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this._httpClient.get(BASE_URL + path, { params }).pipe(
        tap(data=>console.log('Display Data:' + JSON.stringify(data))),
        catchError(this.formatErrors)
      );
  }

  public put(path: string, body: object = {}): Observable<any> {
    return this._httpClient
      .put(BASE_URL + path, JSON.stringify(body), this.options)
      .pipe(catchError(this.formatErrors));
  }

  public post(path: string, body: object = {}): Observable<any> {
    return this._httpClient
      .post(BASE_URL + path, JSON.stringify(body), this.options)
      .pipe(catchError(this.formatErrors));
  }

  public delete(path: string): Observable<any> {
    return this._httpClient.delete(BASE_URL + path).pipe(catchError(this.formatErrors));
  }

  public formatErrors(error: any): Observable<any> {
    return throwError(error.error);
  }
}
