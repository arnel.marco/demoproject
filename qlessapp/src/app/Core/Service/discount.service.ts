import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable, Subject } from 'rxjs';
import { ICardData } from '../DataModel/card.data';
import { map, mergeMap, takeUntil, toArray } from 'rxjs/operators';
import { IDiscountData } from '../DataModel/discount.data';

const routes = {
  Discount : '/Discount',
  DiscountId: (discountId: number) => `/Discount/${discountId}`,
};

@Injectable({
  providedIn: 'root'
})
export class DiscountService {

  constructor(private _apiService: ApiService) { }

  create(discount : IDiscountData) : Observable<IDiscountData>  {
    return this._apiService.post(routes.Discount,discount)
  }

  getAll(): Observable<IDiscountData[]> {
     return this._apiService.get(routes.Discount);
  }

  getById(discountId:number): Observable<IDiscountData> {
    return this._apiService.get(routes.DiscountId(discountId));
  }

  edit(discount:IDiscountData): Observable<IDiscountData> {
    return this._apiService.put(routes.Discount,discount);
  }

  delete(discountId:number): Observable<IDiscountData> {
    return this._apiService.delete(routes.DiscountId(discountId));
  }

}
