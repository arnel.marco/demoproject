import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable, Subject } from 'rxjs';
import { ICardType } from '../DataModel/card.type';

const routes = {
  CardType : '/CardType',
  CardTypeId: (cardTypeId: number) => `/CardType/${cardTypeId}`,
};

@Injectable({
  providedIn: 'root'
})
export class CardtypeService {

  constructor(private _apiService: ApiService) { }

  create(card : ICardType) : Observable<ICardType>  {
    return this._apiService.post(routes.CardType,card)
  }

  getAll(): Observable<ICardType[]> {
     return this._apiService.get(routes.CardType);
  }

  getById(cardTypeId:number): Observable<ICardType> {
    return this._apiService.get(routes.CardTypeId(cardTypeId));
  }

  edit(card:ICardType): Observable<ICardType> {
    return this._apiService.put(routes.CardType,card);
  }

  delete(cardTypeId:number): Observable<ICardType> {
    return this._apiService.delete(routes.CardTypeId(cardTypeId));
  }

}
