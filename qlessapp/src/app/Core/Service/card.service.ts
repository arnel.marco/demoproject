import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable, Subject } from 'rxjs';
import { ICardData } from '../DataModel/card.data';
import { map, mergeMap, takeUntil, toArray } from 'rxjs/operators';

const routes = {
  Card : '/Card',
  CardId: (cardId: number) => `/Card/${cardId}`,
};

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(private _apiService: ApiService) { }

  create(card : ICardData) : Observable<ICardData>  {
    return this._apiService.post(routes.Card,card)
  }

  getAll(): Observable<ICardData[]> {
     return this._apiService.get(routes.Card);
  }

  getById(cardId:number): Observable<ICardData> {
    return this._apiService.get(routes.CardId(cardId));
  }

  edit(card:ICardData): Observable<ICardData> {
    return this._apiService.put(routes.Card,card);
  }

  delete(cardId:number): Observable<ICardData> {
    return this._apiService.delete(routes.CardId(cardId));
  }

}
