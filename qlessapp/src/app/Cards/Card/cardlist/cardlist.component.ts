import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs/internal/Subject';
import { ICardData } from 'src/app/Core/DataModel/card.data';
import { ICardType } from 'src/app/Core/DataModel/card.type';
import { CardService } from 'src/app/Core/Service/card.service';
import { CardtypeService } from 'src/app/Core/Service/cardtype.service';

const newRecord = 'new';
const viewRecord = 'view';

@Component({
  selector: 'app-cardlist',
  templateUrl: './cardlist.component.html',
  styleUrls: ['./cardlist.component.css']
})
export class CardlistComponent implements OnInit,OnDestroy {
  private unSubcribe$ = new Subject();
  mode : string = viewRecord;
  cardId: number = 0;
  card : ICardData;
  cardList: ICardData[] = [];
  cardTypes : ICardType[] = [];

  constructor(private cardService: CardService,
    private modalService : NgbModal,
    private toastrService : ToastrService,
    private cardtypeService: CardtypeService) { }

  ngOnInit() {
    this.getAll();
    this.getAllCardTypes();
  }

  getAll() {
    this.cardService.getAll()
    .pipe(takeUntil(this.unSubcribe$))
    .subscribe(data => {
        this.cardList = data;
    },
    error =>  {
      console.log(error);
      this.toastrService.error(error);
    });
  }

  getAllCardTypes()  {
    this.cardtypeService.getAll()
    .pipe(
      takeUntil(this.unSubcribe$)
    )
    .subscribe(data => {
      this.cardTypes = data;
    },
    error => this.toastrService.error(error));
  }

  delete(cardId: number) {
      this.cardService.delete(cardId)
      .pipe(takeUntil(this.unSubcribe$))
      .subscribe(data => {
        console.log(`Successfully delete :${data}`);
        this.toastrService.info('Successfully delete');
      },
      error => console.log(error));
  }

  openDialogAdd(e: any, content: Component): void {
    this.mode = newRecord;
    let modalRef = this.modalService.open(content, { size: 'lg' });
    modalRef.result.then(() => { this.getAll(); }, () => {})
  }

  openDialogView(e: any, content: Component, cardData : ICardData): void {
      if(cardData.cardId != 0) {
        this.mode = viewRecord;
        this.cardId = cardData.cardId;
        this.card = cardData;
        let modalRef = this.modalService.open(content, { size: 'lg' });
        modalRef.result.then(() => {this.getAll(); }, () => { })
      }
  }

  ngOnDestroy(): void {
    this.unSubcribe$.next();
    this.unSubcribe$.complete();
  }
}
