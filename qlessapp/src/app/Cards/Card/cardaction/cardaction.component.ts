import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ICardData } from 'src/app/Core/DataModel/card.data';
import { ICardType } from 'src/app/Core/DataModel/card.type';
import { CardService } from 'src/app/Core/Service/card.service';
import { DatePipe } from '@angular/common'

const newRecord = 'new';
const viewRecord = 'view';

@Component({
  selector: 'app-cardaction',
  templateUrl: './cardaction.component.html',
  styleUrls: ['./cardaction.component.css'],
  providers: [DatePipe]
})

export class CardactionComponent implements OnInit,OnDestroy {

  @Input() cardInput : ICardData;
  @Input() mode: string = newRecord;
  @Input() cardTypes : ICardType[] = [];

  form: FormGroup;
  private unSubcribe$ = new Subject();

  constructor( private cardService : CardService,
     private fb: FormBuilder,
     private toastrService : ToastrService,
     private datePipe : DatePipe,
     ) {
  }

  ngOnInit() {
    this.createFormGroup();
    if (this.mode != newRecord) {
      const issuedDate = this.datePipe.transform(new Date(this.cardInput.issuedDate), 'yyy-MM-dd');
      const expiryDate = this.datePipe.transform(new Date(this.cardInput.expiryDate), 'yyy-MM-dd');
      this.form.patchValue({
        cardId : this.cardInput.cardId,
        expiryDate : expiryDate,
        issuedDate : issuedDate,
        numberYearsValid : this.cardInput.numberYearsValid,
        load : this.cardInput.load,
        cardType : this.cardInput.cardType,
        isDeleted : this.cardInput.isDeleted,
        recordStatus : viewRecord,
      });
    }
  }

  private createFormGroup() {
    this.form = this.fb.group({
        cardId:[ this.mode == newRecord ? 0 : this.cardInput.cardId],
        issuedDate:[this.mode == newRecord ? '': this.cardInput.issuedDate,Validators.required],
        expiryDate:[this.mode == newRecord ? '' : this.cardInput.expiryDate,Validators.required],
        numberYearsValid: [this.mode == newRecord ? 0 : this.cardInput.numberYearsValid,[Validators.required,Validators.min(1),Validators.max(5)]],
        load: [this.mode == newRecord ? 100 : this.cardInput.load,[Validators.required]],
        cardType: [this.mode == newRecord ? null : this.cardInput.cardType,[Validators.required]],
        isDeleted: [this.mode == newRecord ? false: this.cardInput.isDeleted],
        recordStatus : [this.mode == newRecord ? newRecord: viewRecord],
    });
  }

  insert(card: ICardData)  {
      this.cardService.create(card)
      .pipe(
        takeUntil(this.unSubcribe$)
      )
      .subscribe(data => {
        console.log(`Successfully Created :${JSON.stringify(data)}`);
        this.toastrService.info('Successfully Created');
      },
      error => this.toastrService.error(JSON.stringify(error)));
  }

  update(card: ICardData)  {
      this.cardService.edit(card)
      .pipe(
        takeUntil(this.unSubcribe$)
      )
      .subscribe(data => {
        console.log(`Successfully Updated :${JSON.stringify(data)}`);
        this.toastrService.info('Successfully Updated');
      },
      error => this.toastrService.error(JSON.stringify(error)));
  }

  onSubmit()
  {
    if (!this.form.valid) {
      this.toastrService.warning("Invalid Form data");
      return;
    }

    let request : ICardData = Object.assign({}, this.form.value);
    if (this.mode == newRecord) {
      if (confirm("Are you sure to Insert ?"  )) {
        this.insert(request);
      }
    }
    else {
      if (confirm("Are you sure to Update ?"  )) {
        this.update(request);
      }
    }
  }

  ngOnDestroy(): void {
    this.unSubcribe$.next();
    this.unSubcribe$.complete();
  }
}
