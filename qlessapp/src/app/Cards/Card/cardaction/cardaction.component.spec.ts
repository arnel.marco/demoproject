import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CardactionComponent } from './cardaction.component';


describe('CardactionComponent', () => {
  let component: CardactionComponent;
  let fixture: ComponentFixture<CardactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
