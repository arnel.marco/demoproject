import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { ApiService } from '../Core/Service/api.service';
import { CardService } from '../Core/Service/card.service';
import { CardtypeService } from '../Core/Service/cardtype.service';
import { SharedModule } from '../shared.module';
import { CardRoutingModule } from './card-routing.module';
import { CardactionComponent } from './Card/cardaction/cardaction.component';
import { CardlistComponent } from './Card/cardlist/cardlist.component';
import { CardtypeactionComponent } from './CardType/cardtypeaction/cardtypeaction.component';
import { CardtypelistComponent } from './CardType/cardtypelist/cardtypelist.component';

@NgModule({
  declarations: [
    CardactionComponent,
    CardlistComponent,
    CardtypelistComponent,
    CardtypeactionComponent
  ],
  imports: [
      SharedModule,
      CardRoutingModule,
      ToastrModule.forRoot()
  ],
  providers: [
    CardService,
    CardtypeService,

  ],
  exports : [
  ]
})
export class CardModule { }
