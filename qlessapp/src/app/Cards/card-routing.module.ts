import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardlistComponent } from './Card/cardlist/cardlist.component';
import { CardtypelistComponent } from './CardType/cardtypelist/cardtypelist.component';


const routes: Routes = [
  {
    path: 'Card/List',
    component: CardlistComponent
  },
  {
    path: 'CardType/List',
    component: CardtypelistComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardRoutingModule { }
