import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ICardType } from 'src/app/Core/DataModel/card.type';
import { CardtypeService } from 'src/app/Core/Service/cardtype.service';


const newRecord = 'new';
const viewRecord = 'view';

@Component({
  selector: 'app-cardtypeaction',
  templateUrl: './cardtypeaction.component.html',
  styleUrls: ['./cardtypeaction.component.css'],
})

export class CardtypeactionComponent implements OnInit,OnDestroy {

  @Input() cardType : ICardType;
  @Input() mode: string = newRecord;

  form: FormGroup;
  private unSubcribe$ = new Subject();

  constructor( private cardTypeService : CardtypeService,
     private fb: FormBuilder,
     private toastrService : ToastrService
     ) {
  }

  ngOnInit() {
    this.createFormGroup();
    debugger;
    if (this.mode != newRecord) {
      this.form.patchValue({
        cardTypeId : this.cardType.cardTypeId,
        name : this.cardType.name,
        isDeleted : this.cardType.isDeleted,
        recordStatus : viewRecord,
      });
    }
  }

  private createFormGroup() {
    this.form = this.fb.group({
      cardTypeId:[ this.mode == newRecord ? 0 : this.cardType.cardTypeId],
      name:[this.mode == newRecord ? '': this.cardType.name,Validators.required],
      isDeleted:[this.mode == newRecord ? false : this.cardType.isDeleted],
      recordStatus : [this.mode == newRecord ? newRecord: viewRecord],
    });
  }

  insert(card: ICardType)  {
      this.cardTypeService.create(card)
      .pipe(takeUntil(this.unSubcribe$)
      )
      .subscribe(data => {
        console.log(`Successfully Created :${JSON.stringify(data)}`);
        this.toastrService.info('Successfully Created');
      },
      error => this.toastrService.error(JSON.stringify(error)));
  }

  update(card: ICardType)  {
      this.cardTypeService.edit(card)
      .pipe(takeUntil(this.unSubcribe$)
      )
      .subscribe(data => {
        console.log(`Successfully Updated :${JSON.stringify(data)}`);
        this.toastrService.info('Successfully Updated');
      },
      error => this.toastrService.error(JSON.stringify(error)));
  }

  onSubmit()
  {
    if (!this.form.valid) {
      this.toastrService.warning("Invalid Form data");
      return;
    }

    let request : ICardType = Object.assign({}, this.form.value);
    if (this.mode == newRecord) {
      if (confirm("Are you sure to Insert ?"  )) {
        this.insert(request);
      }
    }
    else {
      if (confirm("Are you sure to Update ?"  )) {
        this.update(request);
      }
    }
  }

  ngOnDestroy(): void {
    this.unSubcribe$.next();
    this.unSubcribe$.complete();
  }
}
