import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CardtypeactionComponent } from './cardtypeaction.component';



describe('CardtypeactionComponent', () => {
  let component: CardtypeactionComponent;
  let fixture: ComponentFixture<CardtypeactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardtypeactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardtypeactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
