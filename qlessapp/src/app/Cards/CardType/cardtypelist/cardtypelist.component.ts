import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs/internal/Subject';
import { ICardType } from 'src/app/Core/DataModel/card.type';
import { CardtypeService } from 'src/app/Core/Service/cardtype.service';

const newRecord = 'new';
const viewRecord = 'view';

@Component({
  selector: 'app-cardtypelist',
  templateUrl: './cardtypelist.component.html',
  styleUrls: ['./cardtypelist.component.css']
})
export class CardtypelistComponent implements OnInit,OnDestroy {
  private unSubcribe$ = new Subject();
  mode : string = viewRecord;
  carTypeId: number = 0;
  cardType : ICardType;
  cardTypes : ICardType[] = [];

  constructor(
    private modalService : NgbModal,
    private toastrService : ToastrService,
    private cardtypeService: CardtypeService) { }

  ngOnInit() {
    this.getAllCardTypes();
  }

  getAllCardTypes()  {
    this.cardtypeService.getAll()
    .pipe(
      takeUntil(this.unSubcribe$)
    )
    .subscribe(data => {
      this.cardTypes = data;
    },
    error => this.toastrService.error(error));
  }

  delete(cardTypeId: number) {
      this.cardtypeService.delete(cardTypeId)
      .pipe(takeUntil(this.unSubcribe$))
      .subscribe(data => {
        console.log(`Successfully delete :${data}`);
        this.toastrService.info('Successfully delete');
      },
      error => console.log(error));
  }

  openDialogAdd(e: any, content: Component): void {
    this.mode = newRecord;
    let modalRef = this.modalService.open(content, { size: 'lg' });
    modalRef.result.then(() => { this.getAllCardTypes(); }, () => {})
  }

  openDialogView(e: any, content: Component, cardTypeData : ICardType): void {
      if(cardTypeData.cardTypeId != 0) {
        this.mode = viewRecord;
        this.carTypeId = cardTypeData.cardTypeId;
        this.cardType = cardTypeData;
        let modalRef = this.modalService.open(content, { size: 'lg' });
        modalRef.result.then(() => {this.getAllCardTypes(); }, () => { })
      }
  }

  ngOnDestroy(): void {
    this.unSubcribe$.next();
    this.unSubcribe$.complete();
  }
}
