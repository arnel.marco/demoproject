import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared.module';
import { ToastrModule } from 'ngx-toastr';
import { ApiService } from './Core/Service/api.service';
import { DiscountModule } from './Discounts/discount.module';
import { CardModule } from './Cards/card.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
   SharedModule,
   CardModule,
   DiscountModule,
   AppRoutingModule,
   ToastrModule.forRoot(),

  ],
  bootstrap: [AppComponent],
  providers:[ApiService]
})
export class AppModule { }
