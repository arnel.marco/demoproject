import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DiscountlistComponent } from './Discount/discountlist/discountlist.component';


const routes: Routes = [
  {
    path: 'Discount/List',
    component: DiscountlistComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiscountRoutingModule { }
