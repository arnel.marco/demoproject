import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IDiscountData } from 'src/app/Core/DataModel/discount.data';
import { DiscountService } from 'src/app/Core/Service/discount.service';

const newRecord = 'new';
const viewRecord = 'view';

@Component({
  selector: 'app-discountaction',
  templateUrl: './discountaction.component.html',
  styleUrls: ['./discountaction.component.css'],
})

export class DiscountactionComponent implements OnInit,OnDestroy {

  @Input() discountInput : IDiscountData;
  @Input() mode: string = newRecord;

  form: FormGroup;
  private unSubcribe$ = new Subject();

  constructor( private discountService : DiscountService,
     private fb: FormBuilder,
     private toastrService : ToastrService
     ) {
  }

  ngOnInit() {
    this.createFormGroup();
    if (this.mode != newRecord) {
      this.form.patchValue({
        discountId : this.discountInput.discountId,
        discountName : this.discountInput.discountName,
        numberOfDiscountPerDay : this.discountInput.numberOfDiscountPerDay,
        principalDiscount : this.discountInput.principalDiscount,
        additionalDiscount : this.discountInput.additionalDiscount,
        isDeleted : this.discountInput.isDeleted,
        recordStatus : viewRecord,
      });
    }
  }

  private createFormGroup() {
    this.form = this.fb.group({
      discountId:[ this.mode == newRecord ? 0 :  this.discountInput.discountId],
      discountName:[this.mode == newRecord ? '': this.discountInput.discountName,Validators.required],
      numberOfDiscountPerDay:[this.mode == newRecord ? 0: this.discountInput.numberOfDiscountPerDay,Validators.required],
      additionalDiscount:[this.mode == newRecord ? 0: this.discountInput.additionalDiscount,Validators.required],
      principalDiscount:[this.mode == newRecord ? 0: this.discountInput.principalDiscount,Validators.required],
      isDeleted:[this.mode == newRecord ? false : this.discountInput.isDeleted],
      recordStatus : [this.mode == newRecord ? newRecord: viewRecord],
    });
  }

  insert(discount: IDiscountData)  {
      this.discountService.create(discount)
      .pipe(takeUntil(this.unSubcribe$)
      )
      .subscribe(data => {
        console.log(`Successfully Created :${JSON.stringify(data)}`);
        this.toastrService.info('Successfully Created');
      },
      error => this.toastrService.error(JSON.stringify(error)));
  }

  update(discount: IDiscountData)  {
      this.discountService.edit(discount)
      .pipe(takeUntil(this.unSubcribe$)
      )
      .subscribe(data => {
        console.log(`Successfully Updated :${JSON.stringify(data)}`);
        this.toastrService.info('Successfully Updated');
      },
      error => this.toastrService.error(JSON.stringify(error)));
  }

  onSubmit()
  {
    if (!this.form.valid) {
      this.toastrService.warning("Invalid Form data");
      return;
    }

    let request : IDiscountData = Object.assign({}, this.form.value);
    if (this.mode == newRecord) {
      if (confirm("Are you sure to Insert ?"  )) {
        this.insert(request);
      }
    }
    else {
      if (confirm("Are you sure to Update ?"  )) {
        this.update(request);
      }
    }
  }

  ngOnDestroy(): void {
    this.unSubcribe$.next();
    this.unSubcribe$.complete();
  }
}
