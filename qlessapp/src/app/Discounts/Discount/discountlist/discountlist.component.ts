import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs/internal/Subject';
import { DiscountService } from 'src/app/Core/Service/discount.service';
import { IDiscountData } from 'src/app/Core/DataModel/discount.data';

const newRecord = 'new';
const viewRecord = 'view';

@Component({
  selector: 'app-discountlist',
  templateUrl: './discountlist.component.html',
  styleUrls: ['./discountlist.component.css']
})
export class DiscountlistComponent implements OnInit,OnDestroy {
  private unSubcribe$ = new Subject();
  mode : string = viewRecord;
  discountId: number = 0;
  discountData : IDiscountData;
  discounts : IDiscountData[] = [];

  constructor(
    private modalService : NgbModal,
    private toastrService : ToastrService,
    private discountService: DiscountService) { }

  ngOnInit() {
    this.getAllDiscount();
  }

  getAllDiscount()  {
    this.discountService.getAll()
    .pipe(
      takeUntil(this.unSubcribe$)
    )
    .subscribe(data => {
      this.discounts = data;
    },
    error => this.toastrService.error(error));
  }

  delete(discountId: number) {
      this.discountService.delete(discountId)
      .pipe(takeUntil(this.unSubcribe$))
      .subscribe(data => {
        console.log(`Successfully delete :${data}`);
        this.toastrService.info('Successfully delete');
      },
      error => console.log(error));
  }

  openDialogAdd(e: any, content: Component): void {
    this.mode = newRecord;
    let modalRef = this.modalService.open(content, { size: 'lg' });
    modalRef.result.then(() => { this.getAllDiscount(); }, () => {})
  }

  openDialogView(e: any, content: Component, discountParam : IDiscountData): void {
      if(discountParam.discountId != 0) {
        this.mode = viewRecord;
        this.discountId = discountParam.discountId;
        this.discountData = discountParam;
        let modalRef = this.modalService.open(content, { size: 'lg' });
        modalRef.result.then(() => {this.getAllDiscount(); }, () => { })
      }
  }

  ngOnDestroy(): void {
    this.unSubcribe$.next();
    this.unSubcribe$.complete();
  }
}
