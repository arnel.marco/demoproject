import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { DiscountService } from '../Core/Service/discount.service';
import { SharedModule } from '../shared.module';
import { DiscountRoutingModule } from './discount-routing.module';
import { DiscountactionComponent } from './Discount/discountaction/discountaction.component';
import { DiscountlistComponent } from './Discount/discountlist/discountlist.component';

@NgModule({
  declarations: [
    DiscountlistComponent,
    DiscountactionComponent
  ],
  imports: [
      SharedModule,
      DiscountRoutingModule,
      ToastrModule.forRoot()
  ],
  providers: [
    DiscountService
  ],
  exports : [
  ]
})
export class DiscountModule { }
