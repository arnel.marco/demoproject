import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// {
//   path: 'cards',
//   loadChildren: './Cards/card.module#CardModule'
// },
const routes: Routes = [
  { path: '', redirectTo: 'cards', pathMatch: 'full' },
  { path: '**', redirectTo: 'cards', pathMatch: 'full'}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {  }
